# Doxygen Awesome

## Overview

This project is designed to automate the generation and deployment of multi-version documentation using Doxygen. It includes configuration files, custom CMake modules, and utilities to facilitate local development, documentation generation, and version management.

## Features

- Automated generation of Doxygen documentation from source code comments.
- Local development server (`doxygen-serve`) for previewing generated documentation.
- Versioned documentation deployment with symlink support for easy access to the latest documentation.
- Customizable HTML templates and resource directories for enhancing the appearance and functionality of the generated documentation.

# Installation

To use this project, ensure you have CMake (version 3.5 or higher) installed. Clone the repository and configure it using CMake. Make sure to have Doxygen installed on your system for generating documentation.

## Usage

1. Clone the repository:
```bash
git clone <repository_url>
cd Doxygen/Awesome
```

2. Configure and build the project using CMake:
```bash
cmake .
make
```

3. Generate Doxygen documentation:
```bash
make doxygen
```

4. Serve documentation locally for preview and automatic updates.
```bash
make serve
```

5. Access generated documentation:
- Generated documentation will be available in the `./docs` directory.
- Latest version can be accessed via `./docs/latest` symlink.

# Documentation

For detailed documentation, including API references and project structure, refer to the generated Doxygen documentation.

## Directory Structure

- `bin/`: Contains utilities like `doxygen-serve` for local documentation serving.
- `cmake/`: Custom CMake modules used for project configuration.
- `css/`, `images/`, `js/`: Directories containing resources for enhancing HTML documentation output.

# Contributing

Contributions are welcome! Fork the repository, create a new branch, make your changes, and submit a pull request. Please follow the project's coding conventions and document any new features or changes.

# License

This project is licensed under MIT License. See the LICENSE file for more details.

# Contact

For questions, feedback, or support, please contact 

# Authors and Acknowledgment

For further support or questions, please contact:
- @[Marco Meyer-Conde](mailto:marco@tcu.ac.jp)
